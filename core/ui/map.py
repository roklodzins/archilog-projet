from kivy.core.image import Image
from kivy.graphics import Fbo, Rectangle, Line, ClearColor, Color, Point, Triangle, Mesh
from kivy.graphics.opengl import glLineWidth
from kivy.graphics.tesselator import Tesselator
from kivy.properties import BooleanProperty
from kivy.resources import resource_find
from kivy.uix.widget import Widget

from core.simu import Country, Territory, Frontier
from core.simu import Coordinate
import warnings as w


class Map(Fbo):

    def __init__(self, frontiers=False, **kwargs):
        self.background = Image(resource_find("ESU.png"))
        self.fron_cache = {}
        self.tess_cache = {}
        self._frontiers = frontiers
        self._selected_country = None
        self._hover_country = None

        super().__init__(size=self.background.size, **kwargs)
        self.frontiers_fbo = Fbo(size=self.size)
        a, b = self.size
        self.rsz = lambda x, y: (a * x, b * y)

        glLineWidth(3)

        self.add_reload_observer(self._reset)
        self._reset()

        self.frontiers_fbo.add_reload_observer(self._render_frontiers_fbo)
        self._render_frontiers_fbo()

    @property
    def frontiers(self):
        return self._frontiers

    @frontiers.setter
    def frontiers(self, value):
        if value != self._frontiers:
            self._frontiers = value
            self._draw()

    @property
    def selected_country(self):
        return self._selected_country

    @selected_country.setter
    def selected_country(self, value):
        if value != self._selected_country:
            self._selected_country = value
            self._draw()

    @property
    def hover_country(self):
        return self._hover_country

    @hover_country.setter
    def hover_country(self, value):
        if value != self._hover_country:
            self._hover_country = value
            self._draw()



    def _reset(self):
        self.before.clear()

        with self.before:
            Rectangle(size=self.size, texture=self.background.texture)

        self._draw()


    def _draw(self):
        self.clear()
        # print("draw", self._selected_country, self._hover_country)

        with self:
            if self._frontiers:
                Rectangle(size=self.size, texture=self.frontiers_fbo.texture)

            if self._selected_country is not None:
                Color(0, 1, 1, 0.4)
                self.fill_zone(Country[self._selected_country])

            if self._hover_country is not None and self._hover_country != self._selected_country:
                Color(1, 1, 1, 0.3)
                self.fill_zone(Country[self._hover_country])

    def _render_frontiers_fbo(self):
        self.frontiers_fbo.clear()

        from random import random as r
        with self.frontiers_fbo:
            Color(1, 1, 1, 1)
            for i in Territory:
                i: Territory
                # Color(r(), r(), r(), 1)
                # Line(points=tuple(c for u in i.outer for c in self.rsz(*u.coord2d)))
                # for u in i.inners: Line(points=tuple(c for u in u for c in self.rsz(*u.coord2d)))
                for u in i:
                    self.render_frontiers(u)

        self.frontiers_fbo.draw()

    def render_frontiers(self, frontier: Frontier):
        from random import random as r
        Color(r(), r(), r(), 1)
        Line(points=self._get_frontiers(frontier))

    def _get_frontiers(self, frontier: Frontier):
        if fron := self.fron_cache.get(frontier.id):
            return fron
        else:
            i = tuple(c for u in frontier for c in self.rsz(*u.coord2d))
            self.fron_cache[frontier.id] = i
            return i

    def fill_zone(self,  zone: Country | Territory):

        if isinstance(zone, Territory):
            zone = [zone]

        for territory in zone:

            if self.tess_cache.get(territory.id):
                tess = self.tess_cache[territory.id]

            else:
                tess = Tesselator()
                o = tuple(c for u in territory.outer for c in self.rsz(*u.coord2d))
                i = tuple(tuple(c for u in inner for c in self.rsz(*u.coord2d)) for inner in territory.inners)
                tess.add_contour(o)

                for n in i: tess.add_contour(n)

                if not tess.tesselate():
                    w.warn(f"Tesselation failed with Territory {territory.id}")

                self.tess_cache[territory.id] = tess

            for vertices, indices in tess.meshes:
                Mesh(vertices=vertices, indices=indices, mode="triangle_fan")
