from kivy import Config

Config.set("graphics", "resizable", "1")
from kivymd.app import MDApp as App
from kivy.lang import Builder
from core.ui.earth import Earth
from core.ui.breakingnews import BreakingNews
from core.ui.nav import Nav, Rail, RailButton
from core.simu import Country
from os.path import dirname




class SimuApp(App):

    def build(self):
        self.title = "Rire & Empire"
        self.theme_cls.theme_style = "Dark"
        self.theme_cls.primary_palette = "Teal"
        return Builder.load_file(dirname(__file__) + "/layout/app.kv")
