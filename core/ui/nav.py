from kivy.properties import ObjectProperty, StringProperty, NumericProperty, AliasProperty
from kivymd.uix.boxlayout import MDBoxLayout
from kivymd.uix.button import MDRectangleFlatIconButton, MDIconButton
from kivymd.uix.list import TwoLineRightIconListItem
from kivymd.uix.navigationdrawer import MDNavigationDrawer

from core.simu import Country


class RailButton(MDIconButton):

    name = StringProperty("")

    def on_release(self):
        self.parent.nav.sm.current = self.name.lower()
        self.parent.nav.set_state("open")


class Rail(MDBoxLayout):
    nav = ObjectProperty(None)

MDIconButton
class CountryItem(TwoLineRightIconListItem):
    country = ObjectProperty(None)
    icon = StringProperty("")
    nav = ObjectProperty(None)

    def on_country(self, w, country):
        self.text = country.name
        self.secondary_text = f"Placeholder: CT#{country.id}"
        #self.icon = country.flag

    def on_release(self):
        self.nav.sm.current = "stats"
        self.nav.selected_country = self.country.id


class Nav(MDNavigationDrawer):

    sm = ObjectProperty(None)
    list = ObjectProperty(None)
    selected_country = NumericProperty(None, allownone=True)

    country = AliasProperty(lambda s: s.selected_country and Country[s.selected_country], None, bind=["selected_country"])

    def search(self, name):
        self.list.clear_widgets()
        name = name.lower()
        for i in Country:
            i: Country
            if any(u.startswith(name) for u in i.keywords):
                self.list.add_widget(CountryItem(country=i, nav=self))
