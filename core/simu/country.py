from __future__ import annotations

from typing import Any, Generator, Iterator

from core.simu.coord import Coordinate
from core.simu.territory import Territory
import random, os, json

__all__ = (
    "Country",
)

from core.simu.utils import JsonEncoder


class MetaCountry(type):
    All: dict[Country] = {}

    def __getitem__(cls, item) -> Country:  # Make Country sliceable
        return cls.All[item]

    def __iter__(cls) -> Iterator[Country]:
        return iter(cls.All.values())


class Country(metaclass=MetaCountry):
    """Represents a country"""

    __slots__ = (
        "_id",
        "_territories",
        "_center",
        "_name",
        "__keywords",

        "_birth_rate",
        "_death_rate",

        "_pib",
        "_eco_growth",
        "_eco_stability",

        "_militarization",
        "_army_size",
        "_army_budget",

        "_politiscale",
        "_stability",

        "_soft_power",

        "_admin_by",
        "_admin_list"
    )

    mat_prox = []
    mat_ally = []

    @classmethod
    def load(cls, countries: str = os.path.dirname(__file__) + "/database/new_countries.json"):

        with open(countries, "r") as r:
            ct = json.load(r)

        for i in ct:
            cls.All[i["id"]] = cls(**i)

    @classmethod
    def save(cls, countries: str = os.path.dirname(__file__) + "/database/countries_SV1.json"):

        with open(countries, "w") as w:
            w.write(JsonEncoder(indent = 4).encode(list(cls)))

    @classmethod
    def get(cls, coord: Coordinate) -> Country:
        for i in cls:
            i: Country
            if coord in i:
                return i

    def __init__(self, **kw):
        try:
            for i in self.__slots__:
                if i[1] != "_":
                    setattr(self, i, kw[i[1:]])
        except KeyError as e:
            raise KeyError(f"Missing key: {e}") from None

        # print(self.center, len(self))

        self.__keywords = {u for i in self._name.lower().split(" ") for u in i.split('-')}

        #print(f"\nCountry {self._name} Loaded")
        self._center = Coordinate(*self._center)

    @property
    def id(self) -> int:
        return self._id

    @property
    def name(self) -> str:
        return self._name

    @property
    def keywords(self) -> set[str]:
        return self.__keywords

    @property
    def territories(self) -> list[int]:
        return self._territories

    @property
    def capital(self) -> int:
        return self._territories[0]

    @property
    def capital_territory(self) -> Territory:
        return Territory[self.capital]

    @property
    def center(self):
        return self._center

    @property
    def population(self):
        return sum((i.population for i in self))

    @property
    def surface(self):
        return sum((i.surface for i in self))

    def population_grow(self):
        for i in self:
            i.population_grow(self._birth_rate, self._death_rate)

    def economy_grow(self):
        if random.random() < self._eco_stability:
            self._pib *= self._eco_growth
        else:
            pass

    def __len__(self):
        return len(self._territories)

    def __getitem__(self, item) -> Territory:
        return Territory[self._territories[item]]

    def __iter__(self) -> Generator[Territory, Any, None]:
        return (Territory[i] for i in self._territories)

    def __contains__(self, item):
        if isinstance(item, Country):
            r = item.id in self._admin_list

        elif isinstance(item, Territory):
            r = item in self._territories

        elif isinstance(item, Coordinate):
            r = any(map(lambda x: item in x, self))
        else: r = False
        # if r: print(item, "in", self._name)
        return r

    def __json__(self) -> dict:
        return {i[1:]: getattr(self, i) for i in self.__slots__}
