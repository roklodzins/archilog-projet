from country import *

__all__ = (
    "Simulation",
)


class Simulation:
    __slots__ = (
        "_year",
        "_month",
        "_speed",
        "_world"
    )

    def __init__(self, year: int, month: int, speed: int):
        self._year = year
        self._month = month
        self._speed = speed

    def simulate_month(self):
        for country in self._world:
            country.population_grow()
            country.economy_grow()
        self._month += 1
        if self._month >= 12:
            self._month -= 12
            self._year += 1

