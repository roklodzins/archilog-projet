import json


__all__ = (
    "JsonEncoder",
)


class JsonEncoder(json.JSONEncoder):
    def default(self, obj):
        if hasattr(obj, "__json__"): return obj.__json__()

        # Let the base class default method raise the TypeError
        return json.JSONEncoder.default(self, obj)
