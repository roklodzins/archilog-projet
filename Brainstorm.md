# TP2 Archi_log

## Idées

    -Simulateur Géopolitique

### Stats Pays

    float Stabilité Politique

    float Poids historique
    float Puissance Militaire                         //Seuil atomique
    float Puissance économique
    float Soft Power
    float neutralité     
    float Politiscale                                //(droite gauche)(autoritaire libertaire)
    float vieillissement
    int Population
    int Surface
    Regime regime

    float[] matrice alliance
    float[] proximité
    

    allianceAvec(Pays)
    proximité(Pays)


### Déroulement

    une itération = 1 mois ? 6 mois ? 1 an ?

    à chaque itération :
        - On tire un nombre aléatoire d'événements
        - On tire ces événements dans une liste d'événements
        - Pour chaque évènement,  on tire un nombre de pays selon le nombre d'arguments requis (importance de proximité).
        - On réalise les évènements
    
### Nom

    Simul'Nation
    Polémicator
    Polémique Simulator
    Geopolitical simulator
    The Only Really Accurate Geo Sim

    TORAGS

    Geopolical
    Legend
    Of
    Brilliantly
    Accurate
    Likeness

### Evenements

    - Putsh : 1
    - Coup d'état : 2 (pays influencé)
    - Catastrophe naturelle : 1
    - Extradition : 2
    - Sommet international : 10
    - Rencontre Diplomatique : 2
    - Capture d'espion : 2
    - Detection de sous marin : 2
    - Crise Bancaire : 1
    - Fete nationale : 1
    - Pandemie : Global
    - Rencontre Sportive Majeure : 2
    - Plan spatiale : 1
    - Attentat : 1
    - Attentat : 2
    - Election : 1
    - Genocide : 2

### Régime

    Democratie
    Dictature
    Monarchie



